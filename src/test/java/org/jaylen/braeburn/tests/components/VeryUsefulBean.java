package org.jaylen.braeburn.tests.components;

import org.jaylen.braeburn.annotations.Component;
import org.jaylen.braeburn.annotations.Requirement;

@Component(role = UsefulBean.class, id = "very")
public class VeryUsefulBean implements UsefulBean {
    @Requirement(id = "some")
    private UsefulBean bean;
}
