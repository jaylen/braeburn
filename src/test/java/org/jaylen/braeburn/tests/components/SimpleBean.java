package org.jaylen.braeburn.tests.components;

import org.jaylen.braeburn.annotations.Component;
import org.jaylen.braeburn.annotations.Requirement;

@Component(role = SimpleBean.class)
public class SimpleBean {

    @Requirement(id = "very")
    private UsefulBean bean;

}
