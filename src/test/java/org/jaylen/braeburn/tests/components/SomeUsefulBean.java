package org.jaylen.braeburn.tests.components;

import org.jaylen.braeburn.annotations.Component;

@Component(role = UsefulBean.class, id = "some")
public class SomeUsefulBean implements UsefulBean {

}
