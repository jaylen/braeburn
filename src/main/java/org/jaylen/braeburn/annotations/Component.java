package org.jaylen.braeburn.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Classes annotated with this annotation are available
 * as beans to be looked up from context and injected
 * into other beans and constructor arguments.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Component {
    Class<?> role();
    String id() default "default";
    boolean singleton() default true;
    boolean lazy() default false;
}
