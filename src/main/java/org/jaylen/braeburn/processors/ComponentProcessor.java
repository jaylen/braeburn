package org.jaylen.braeburn.processors;

import org.jaylen.braeburn.annotations.Component;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.StandardLocation;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.stream.Collectors;

@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes("org.jaylen.braeburn.annotations.Component")
public class ComponentProcessor extends AbstractProcessor {

    @Override
    public synchronized void init(final ProcessingEnvironment environment) {
        super.init(environment);
        filer = environment.getFiler();
        messager = environment.getMessager();
    }

    @Override
    public boolean process(final Set<? extends TypeElement> annotations, final RoundEnvironment environment) {

        if (environment.errorRaised()) {
            messager.printMessage(Diagnostic.Kind.ERROR, "skipping processing since errors have been detected");
        } else if (environment.processingOver()) {
            try {
                final Writer writer = filer.createResource(StandardLocation.CLASS_OUTPUT, "", Constants.BEANS_FILE).openWriter();
                final Set<String> names = entries.stream().sorted().collect(Collectors.toSet());
                for (final String klass : names) {
                    writer.append(klass).append("\n");
                }
                writer.close();
            } catch (IOException e) {
                throw new RuntimeException("unexpected fault", e);
            }
        } else {
            for (final Element element : environment.getElementsAnnotatedWith(Component.class)) {
                if (ElementKind.CLASS.equals(element.getKind())) {
                    entries.add(element.asType().toString());
                }
            }
        }
        return true;
    }

    private Filer filer;

    private Messager messager;

    private List<String> entries = new Vector<>();

}
