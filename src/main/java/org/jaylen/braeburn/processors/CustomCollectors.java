package org.jaylen.braeburn.processors;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public final class CustomCollectors {

    public static <T> Collector<T, ?, Set<T>> toLinkedHashSet() {
        return Collectors.toCollection(LinkedHashSet::new);
    }

}
