package org.jaylen.braeburn.processors;

class BeanEntry<T> {

    private Class<?> role;
    private Class<T> implementation;
    private String id;
    private boolean singleton;
    private boolean lazy;

//    public BeanEntry(final Class<T> implementation) {
//        if (implementation.isAnnotationPresent(Component.class)) {
//            final Component annotation = implementation.getAnnotation(Component.class);
//            this.implementation = implementation;
//            if (annotation.role().isAssignableFrom(implementation)) {
//                this.role = annotation.role();
//                this.id = annotation.id();
//                this.singleton = annotation.singleton();
//                this.lazy = annotation.lazy();
//            } else {
//                throw new NotComponentException("implementation type `" + implementation + "` is not assignable from role type `" + this.role + "`" );
//            }
//        } else {
//            throw new NotComponentException("no component annotation found in implementation type `" + implementation + "`");
//        }
//    }

//    T create(final Object... arguments) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
//        if (created && singleton) {
//            throw new IllegalStateException("singleton bean has allready bean created");
//        } else {
//            final Constructor<T> constructor = implementation.getConstructor(Arrays.stream(arguments).map(Object::getClass).toArray(Class[]::new));
//            this.created = true;
//            return constructor.newInstance(arguments);
//        }
//    }

    Class<?> role() {
        return role;
    }

    Class<T> implementation() {
        return implementation;
    }

    String id() {
        return id;
    }

    boolean singleton() {
        return singleton;
    }

    boolean lazy() {
        return lazy;
    }

}
