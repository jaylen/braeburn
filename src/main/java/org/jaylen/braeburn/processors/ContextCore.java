package org.jaylen.braeburn.processors;

import org.jaylen.braeburn.annotations.Component;
import org.jaylen.braeburn.annotations.Configuration;
import org.jaylen.braeburn.annotations.Requirement;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

class ContextCore {

    protected void scan() {
        final ClassLoader loader = Context.class.getClassLoader();
        final URL url = loader.getResource(Constants.BEANS_FILE);
        if (url == null) {} else {
            try (final BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
                for (final String name : reader.lines().collect(Collectors.toList())) {
                    try {
                        final Class<?> type = Class.forName(name);
                        if (type.isAnnotationPresent(Component.class)) {
                            final Component descriptor = type.getAnnotation(Component.class);
                            final String id = descriptor.id();
                            implementations.put(id, type);
                        }
                    } catch (final ClassNotFoundException fault) {
                        throw new IllegalStateException("class `" + name + "` referenced but cannot be found", fault);
                    }
                }
            } catch (final IOException fault) {
                throw new RuntimeException("unexpected fault while reading resource", fault);
            }
        }
    }

    protected void scan(final Class<?> configuration) {
        if (configuration.isAnnotationPresent(Configuration.class)) {

        }
    }

    protected void build() {
        for (final Class<?> implementation : implementations.values()) {
            final Collection<Requirement> requirements = gather(implementation);
            for (final Requirement requirement : requirements) {
                final String id = requirement.id();
                final Class<?> dep = implementations.get(id);
                final Component descriptor = dep.getAnnotation(Component.class);
                //descriptor.
            }
        }
    }

//    private <T> void create(final Class<T> implementation) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
//        final Collection<Requirement> requirements = gather(implementation);
//        final List<Object> dependencies = new ArrayList<>();
//        for (final Requirement requirement : requirements) {
//            if (beans.containsKey(requirement.id())) {} else {
//                create(implementations.get(requirement.id()));
//            }
//            dependencies.add(beans.get(requirement.id()));
//        }
//        final T bean = implementation.getConstructor().newInstance();
//        BeanUtils.setProperty(bean, );
//    }

    private <T> Collection<Requirement> gather(final Class<T> implementation) {
        return gather(implementation, Collections.singleton(implementation));
    }

    private <T> Collection<Requirement> gather(final Class<T> implementation, final Collection<Class<?>> path) {
        final Collection<Field> fields = getReqFields(implementation);
        final Collection<Requirement> result = new ArrayList<>();
        for (final Field field : fields) {
            final Requirement annotation = field.getAnnotation(Requirement.class);
            final String id = annotation.id();
            if (implementations.containsKey(id)) {
                final Class<?> type = implementations.get(id);
                if (path.contains(type)) {
                    throw new Faults.CircularDependencyException(ContextUtils.join(type, path).collect(Collectors.toList()));
                }
                result.addAll(gather(type, ContextUtils.join(type, path).collect(CustomCollectors.toLinkedHashSet())));
            } else {
                throw new Faults.NoSuchBeanException(id);
            }
            result.add(annotation);
        }
        return result;
    }

    private <T> Collection<Field> getReqFields(final Class<T> implementation) {
        final Collection<Field> current = Arrays.stream(implementation.getDeclaredFields()).filter(f -> f.isAnnotationPresent(Requirement.class)).collect(Collectors.toList());
        final Class<?> sup = implementation.getSuperclass();
        if (sup == null) {
            return current;
        } else {
            return ContextUtils.join(getReqFields(sup), current).collect(Collectors.toList());
        }
    }

//    private <T> void process(final Class<T> implementation) {
//        if (implementation.isAnnotationPresent(Component.class)) {
//            final Component annotation = implementation.getAnnotation(Component.class);
//            if (annotation.role().isAssignableFrom(implementation)) {
//                @SuppressWarnings("unchecked") Class<? super T> role = (Class<? super T>) annotation.role();
//
//                final List<Class<? super T>> parents = new LinkedList<>();
//                Class<? super T> parent = implementation;
//                while (!parent.equals(role)) {
//                    parent = parent.getSuperclass();
//                    parents.add(parent);
//                }
//
//
//
//                String id = annotation.id();
//                boolean singleton = annotation.singleton();
//                boolean lazy = annotation.lazy();
//            } else {
//                throw new Faults.RoleMismatchException(implementation, annotation.role());
//            }
//        } else {
//            throw new Faults.NotComponentException(implementation);
//        }
//    }

    private Map<String, Class<?>> implementations = new ConcurrentHashMap<>();

    private Map<String, Object> beans = new ConcurrentHashMap<>();

}
