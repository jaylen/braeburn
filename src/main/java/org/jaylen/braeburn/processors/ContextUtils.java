package org.jaylen.braeburn.processors;

import org.jaylen.braeburn.annotations.Component;

import java.util.Collection;
import java.util.stream.Stream;

class ContextUtils {

    static <T> boolean isBeanType(final Class<T> implementation) {
        return implementation.isAnnotationPresent(Component.class) && implementation.getAnnotation(Component.class).role().isAssignableFrom(implementation);
    }

    @SafeVarargs
    static <T> Stream<T> join(final Collection<T>... lists) {
        return Stream.of(lists).flatMap(Collection::stream);
    }

    static <T> Stream<T> join(final T item, final Collection<T> items) {
        return Stream.concat(Stream.of(item), items.stream());
    }

    static <T> Stream<T> join(final Collection<T> items, final T item) {
        return Stream.concat(Stream.of(item), items.stream());
    }

}
