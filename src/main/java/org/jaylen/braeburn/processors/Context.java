package org.jaylen.braeburn.processors;

import org.jaylen.braeburn.annotations.Configuration;

import java.util.Arrays;

/**
 * Bean context which allows management of beans.
 */
public class Context extends ContextCore {

    /**
     * Creates instance of {@link Context context} populated with bean
     * information loaded from a static resource (if present) and provided
     * {@link Configuration configuration} classes.
     *
     * @param configurations a (possible empty) sequence of configuration
     *                       classes
     * @return instance of {@link Context context}.
     */
    public static Context create(final Class<?>... configurations) {
        final Context context = new Context();
        Arrays.stream(configurations).forEach(context::scan);
        context.scan();
        context.build();
        return context;
    }


//    public <T> T get(final Class<T> type) {
//        if (index.containsKey(type)) {
//            final Set<String> items = index.get(type);
//            final Optional<String> optional = items.stream().findFirst();
//            if (optional.isPresent() && beans.containsKey(optional.get())) {
//                return type.cast(beans.get(optional.get()));
//            } else {
//                return null;
//            }
//        } else {
//            return null;
//        }
//    }

//    private void scan1() {
//        final ClassLoader loader = Context.class.getClassLoader();
//        try (final InputStream stream = loader.getResourceAsStream(Constants.BEANS_FILE)) {
//            final Properties properties = new Properties();
//            properties.load(stream);
//            for (final Map.Entry<Object, Object> entry : properties.entrySet()) {
//                final String name = (String) entry.getKey();
//                try {
//                    final Class<?> type = Class.forName(name);
//                    final Component component = type.getAnnotation(Component.class);
//                    if (component == null) {
//                        throw new IllegalStateException("missing annotation in component class " + name);
//                    }
//                    final String id = component.id();
//                    final Class<?> role = component.role();
//                    putBean(id, role, createBean(role, type));
//                } catch (final ClassNotFoundException fault) {
//                    throw new IllegalStateException("class " + name + " referenced but cannot be found", fault);
//                }
//            }
//
//        } catch (final IOException fault) {
//            throw new IllegalArgumentException("unable to read resource " + Constants.BEANS_FILE, fault);
//        }
//    }

//    private <R, T> boolean shouldCreate(final String id, final Class<R> role, final Class<T> type))
//
//    {
//
//    }

}
