package org.jaylen.braeburn.processors;

import java.util.Collection;
import java.util.stream.Collectors;

public interface Faults {

    class NoSuchBeanException extends RuntimeException {
        public NoSuchBeanException(final String id) {
            super("no bean with id `" + id + "` found");
        }
    }

    class NotComponentException extends RuntimeException {
        public NotComponentException(final Class<?> implementation) {
            super("implementation type `" + implementation + "` is not annotated as component");
        }
    }

    class RoleMismatchException extends RuntimeException {
        public RoleMismatchException(final Class<?> implementation, final Class<?> role) {
            super("implementation type `" + implementation + "` is not assignable from role type `" + role + "`");
        }
    }

    class CircularDependencyException extends RuntimeException {
        public CircularDependencyException(final Collection<Class<?>> path) {
            super("circular dependency:\n  - " + path.stream().map(Class::getCanonicalName).collect(Collectors.joining(" is required by\n  - ")));
        }
    }

}
